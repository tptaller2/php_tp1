# PHP_TP1

Este es el TP1 de Desarrollo Web con Cristina Ferro Croce.

# Consigna

Diseñar un formulario de contacto en html con css. Debe adaptarse a cualquier tamaño de pantalla, es decir, que debe ser responsive. En el mismo, solicitar el ingreso de los siguientes datos: nombre y apellido, mail del usuario, asunto y mensaje. El propósito del formulario será enviar un correo electrónico al mail mcferrocroce@gmail.com (esto con PHP), que reciba los datos ingresados y los muestre en el cuerpo del mail, excepto asunto que deberá mostrarse como titular del mail.

Se evaluarán en el TP los siguientes puntos:
- Diseño de la interfaz gráfica (que sea funcional y amigable para el usuario). Es decir: no basta con colocar unas cajas de texto y un botón y que sólo funcione...
- Que utilice hojas de estilo css para una muy buena presentación (hagan de cuenta que son diseñadores independientes de páginas web y que están haciendo una presentación para un potencial cliente)
- Que el formulario sea responsive (que se adapte a cualquier tamaño de pantalla)

Pautas para la presentación del TP:
El mismo se presentará en grupos de 3 integrantes.
Para la entrega, pueden manejarse con alguna de las siguientes alternativas:
1) solicitar videoconferencia en la que estarán presentes todos los integrantes del grupo que presenta el trabajo (tener en cuenta que todos los integrantes deberán tener conocimiento completo de todo el proyecto)
2) documentar uds mismos la realización de la página web (por ejemplo realizando una presentación en PowerPoint con audio y convirtiendo luego a video). En este caso, me envían el video obtenido (ojo! Buscar opciones para reducir el tamaño del archivo). Si optan por esta forma de entrega, en el trabajo se deberá poder identificar la participación de cada uno de los integrantes.
Plazo de entrega: 3 semanas a partir de hoy, 30 de abril. 

Al ser este un curso de 3er año, esta es una excelente oportunidad para demostrar y combinar saberes y experiencias adquiridos desde el comienzo de la carrera.

Para tener en cuenta: en próximo TP, se trabajará con base de datos, así que sugiero que vayan repasando lo visto en aquellas materias donde vieron este tema.

## Instalacion

Clonar el repositorio a la carpeta htdocs del XAMPP (i.e. C:\xampp\htdocs\)
Se podrá acceder a la página mediante localhost/php_tp1/contacto.php - siendo php_tp1 la carpeta del proyecto y contacto.php la pantalla principal del proyecto.

Leer el post que contiene los pasos para instalar PHPMailer

https://pepipost.com/tutorials/send-an-email-via-gmail-smtp-server-using-php/

https://github.com/PHPMailer/PHPMailer/archive/master.zip --> el mailer
https://getcomposer.org/ --> lo enlaza al proyecto

## Grupo
Gabriel Maceira Alvarez
Rodrigo Ruiz
Michelle Salinas
Leandro Trillo