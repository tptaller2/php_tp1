<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.0/jquery.waypoints.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="shortcut icon" href="./img/consulta.ico" type="image/x-icon"/>
  <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap" rel="stylesheet">
  <title>Contacto</title>
</head>

<body data-spy="scroll" data-target="#info-nav" class="d-flex flex-column min-vh-100">
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <a class="navbar-brand" href="#">e-m@il</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#info-nav" aria-controls="info-nav"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="info-nav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="#home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#correo">Enviar correo</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="container-fluid">
    <div class="row d-flex justify-content-center introduccion">
    <div class="row" id="home">
      <div class="col-12 fondo-titulo">
        <div class="titulo">e-m@il Cristina</div>
      </div>
    </div>
      <div class="col-12">
        <div class="col-md-8 col-lg-7 texto-intro ml-2 mr-2 mt-5 slide-animate-left primer-linea">
          ¡Gracias por ingresar!<br>
          Mediante esta página podrás enviarle tus consultas a María Cristina Ferro Croce.
        </div>
        <div class="col-md-8 col-lg-7 texto-intro ml-2 mr-2 mt-2 slide-animate-left segunda-linea">
          Simplemente completá el formulario con todos tus datos y clickeá Enviar. Así de sencillo.
        </div>
      </div>
    </div>

    <div class="row d-flex justify-content-around formYTexto align-items-center " id="correo">
      <div class="col-3 col-md-2 sobre-animacion">
        <img src="./img/letter.svg" alt="sobre" class="sobre">
      </div>
      <form class="col-sm-9 col-md-8 col-lg-7 form-correo pt-3" name="formMail" method="POST" action="form.php">
      <div class="col-12 text-center titulo-form pb-2">
        Completá el formulario
      </div>
        <div class="row">
          <div class="form-group col-md-6 col-sm-12">
            <label for="inputNombre">Nombre</label>
            <input type="text" class="form-control" id="inputNombre" name="inputNombre" placeholder="Juan" required>
          </div>

          <div class="form-group col-md-6 col-sm-12">
            <label for="inputApellido">Apellido</label>
            <input type="text" class="form-control" id="inputApellido" name="inputApellido" placeholder="Perez" required>
          </div>
        </div>

        <div class="form-group col-xm-12">
          <label for="inputMail">Correo electrónico</label>
          <input type="email" class="form-control" id="inputMail" name="inputMail" placeholder="tucorreo@ejemplo.com" required>
        </div>

        <div class="form-group col-xm-12">
          <label for="asunto">Asunto</label>
          <input type="text" class="form-control" id="asunto" name="asunto" placeholder="IFTS16 - TP1" required>
        </div>

        <div class="form-group">
          <label for="mensaje">Mensaje</label>
          <textarea class="form-control" id="mensaje" name="mensaje" rows="5" required></textarea>
        </div>
        
        <div class="row d-flex justify-content-center mb-5">
          <input type="submit" class="btn-lg btn-primary">
        </div>
      </form>
    </div>
  </div>
  <footer class="pt-3 pl-3">
  Maceira/Ruiz/Salinas/Trillo - 2020
  </footer>
  <script>

    $('.home').waypoint(function () {
      $('.home').addClass('slide-animate');
    }, { offset: '50%' });

    $('.sobre-animacion').waypoint(function () {
      $('.sobre-animacion').addClass('slide-animate-left');
    }, { offset: '80%' });

    $('.form-correo').waypoint(function () {
      $('.form-correo').addClass('slide-animate-right');
    }, { offset: '75%' });

  </script>
</body>



</html>